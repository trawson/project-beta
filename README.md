# CarCar

Team:

* River - Sales
* Thomas - Service

## Design
Beautimus Bootstrap and React. Display data via tables.
Bootstrap to place data inside form fields. Muted Colors. No installed packages to keep it simple.

## Service microservice
*********************************
---------------------------------
*********************************
FOR TESTING:
Some urls can be tricky. Please use:
    Create Automobile Models URLS:
    F-150:
    https://images.drive.com.au/driveau/image/upload/c_fill,f_auto,g_auto,h_540,q_auto:good,w_960/cms/uploads/hiurlwc2p9ckdud0xn9o
    F-PACE:
    https://s3.us-east-2.amazonaws.com/dealer-inspire-vps-vehicle-images/e362-110009390/SADCJ2EX9PA714987/af7ddc987d2d03ca77ac51aff4c88678.jpg
Thank you!
**********************************
----------------------------------
**********************************

Technician Model
    first_name
    last_name
    employee_id

AutomobileVO
    href
    vin

    def __str__ (self) as well

Appointment Model
    date_time
    reason
    vip
    status (default to 'created')
    vin
    customer
    technician (Foreign Key)

Create Poller

Models will integrate with the inventory microservice through a VO in the models.py which will pull in the href and vin from inventory/api/inventory_rest/models.py



## Sales microservice

First I will need to create the following models
A sales Person model. Containing first name, last name, and employee id field

SalesPerson model
    first_name
    last_name
    employee_id

A customer model. Containing a first name, a last name, address, and a phone number field

CusomterModel
    first_name
    last_name
    address
    phone_number

a Sale Model. Containing a automobile, salesperson, customer, and price field (AutoMobile Salesperson and Customer Should all be in fopreign key fields)

SaleModel
    automobile(foreignKey)
    salesperson(foreignKey)
    customer(foreignKey)
    pricefield


An autombile VO

My poller should be updating every 60 seconds with updated VINs from inventory service.
