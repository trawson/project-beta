from django.contrib import admin

from .models import AutomobileVO, Customer, Sale, Salesperson


@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    pass


@admin.register(Salesperson)
class Salesperson(admin.ModelAdmin):
    pass


@admin.register(Sale)
class Sale(admin.ModelAdmin):
    pass


@admin.register(Customer)
class Customer(admin.ModelAdmin):
    pass
