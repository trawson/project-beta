from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
import json
from .encoders import (
    TechnicianEncoder,
    AppointmentEncoder,
)
from .models import (
    Technician,
    Appointment,
    AutomobileVO
)


@require_http_methods(["GET", "POST"])
def technician_list(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            technicians = Technician.objects.create(**content)
            return JsonResponse(
                {"technicians": technicians},
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "Could not populate Technician Information"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def technician_detail(request, id):
    if request.method == "DELETE":
        technician = get_object_or_404(Technician, id=id)
        count, _ = technician.delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def appointment_list(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            vin = content["vin"]
            if AutomobileVO.objects.filter(vin=vin).exists():
                content['vip'] = "yes"
            else:
                content['vip'] = "no"
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except (Technician.DoesNotExist, AutomobileVO.DoesNotExist):
            return JsonResponse(
                {"error": "Could not Schedule Appointment"},
                status=400
            )


@require_http_methods(["PUT"])
def appointment_cancel(request, id):
    try:
        content = json.loads(request.body)
        status = content["status"]
        Appointment.objects.filter(id=id).update(**content)
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "The Appointment Does Not Exist"})
        response.status_code = 400
        return response


@require_http_methods(["PUT"])
def appointment_complete(request, id):
    try:
        content = json.loads(request.body)
        status = content["status"]
        Appointment.objects.filter(id=id).update(**content)
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "The Appointment Does Not Exist"})
        response.status_code = 400
        return response


@require_http_methods(["DELETE"])
def appointment_details(request, id):
    if request.method == "DELETE":
        appointment = get_object_or_404(Appointment, id=id)
        count, _ = appointment.delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
