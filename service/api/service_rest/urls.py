from django.urls import path
from .views import (
    technician_list,
    technician_detail,
    appointment_list,
    appointment_details,
    appointment_cancel,
    appointment_complete,
)


urlpatterns = [
    path(
        'technicians/',
        technician_list,
        name='technician_list'
    ),
    path(
        'technicians/<int:id>/',
        technician_detail,
        name='technician_detail'
    ),
    path(
        'appointments/',
        appointment_list,
        name='appointment_list'
    ),
    path(
        'appointments/<int:id>',
        appointment_details,
        name='appointment_detail'
    ),
    path(
        'appointments/<int:id>/cancel/',
        appointment_cancel,
        name='appointment_cancel'
    ),
    path(
        'appointments/<int:id>/finish/',
        appointment_complete,
        name='appointment_finish'
    ),
]
