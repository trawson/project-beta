function MainPage() {
    return (
        <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-2 fw-bold">CarGO</h1>
        <div className="col-lg-6 mx-auto">
            <hr></hr>
            <p className="lead mb-4"><b>
            Your premier solution for automobile dealership
            management</b>
            </p>
        </div>
            <img className="imageMain" src="https://di-uploads-pod4.dealerinspire.com/jaguarfreeport/uploads/2019/10/jaguar-f-pace-interior.png" alt="car interior sharp" />
        </div>
    );
}

export default MainPage;
