import React, { useState } from "react";

function NewSalesPersonForm () {
    const[formData, setFormData] = useState({
        first_name: ' ',
        last_name: ' ',
        employee_id: ' ',
    });


    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        })
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const newSalesPersonUrl = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application.json',
            },
        };

        const response = await fetch (newSalesPersonUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                first_name: ' ',
                last_name: ' ',
                employee_id: ' ',
            });
        }
    }


    return (
        <div className="my-5 container">
        <div className="row">
            <form onSubmit={handleSubmit} id="NewSalesPersonForm">
                <h1 className="mb-3">
                    Create New Sales Person
                </h1>
                <div className="form-floating mb-3">
                    <input style={{ backgroundColor: "rgb(228, 230, 240)"}} onChange={handleFormChange} value={formData.first_name}  placeholder="first_name" required type="text" name="first_name" id="first_name" className="form-control" />
                    <label htmlFor="name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input style={{ backgroundColor: "rgb(228, 230, 240)"}} onChange={handleFormChange} value={formData.last_name} placeholder="last_name" required type="text" name="last_name" id="last_name" className="form-control" />
                    <label htmlFor="email">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input style={{ backgroundColor: "rgb(228, 230, 240)"}} onChange={handleFormChange} value={formData.employee_id}  placeholder="employee_id" required type="text" name="employee_id" id="employee_id" className="form-control" />
                    <label htmlFor="address">Employee ID</label>
                </div>
                <button className="btn btn-secondary">Create</button>
                </form>
            </div>
            </div>
)};
export default NewSalesPersonForm;
