import React, { useState } from "react";

function NewCustomerForm () {
    const[formData, setFormData] = useState({
        first_name: ' ',
        last_name: ' ',
        address: ' ',
        phone_number: ' ',
    });


    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        })
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const newCustomerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application.json',
            },
        };

        const response = await fetch (newCustomerUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                first_name: ' ',
                last_name: ' ',
                address: ' ',
                phone_number: ' ',
            });
        }
    }


    return (
        <div className="my-5 container">
        <div className="row">
            <form onSubmit={handleSubmit} id="NewCustomerForm">
                <h1 className="mb-3">
                    Create Customer
                </h1>
                <div className="form-floating mb-3">
                    <input style={{ backgroundColor: "rgb(228, 230, 240)"}} onChange={handleFormChange} value={formData.first_name}  placeholder="first_name" required type="text" name="first_name" id="first_name" className="form-control" />
                    <label htmlFor="name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input style={{ backgroundColor: "rgb(228, 230, 240)"}} onChange={handleFormChange} value={formData.last_name} placeholder="last_name" required type="text" name="last_name" id="last_name" className="form-control" />
                    <label htmlFor="email">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input style={{ backgroundColor: "rgb(228, 230, 240)"}} onChange={handleFormChange} value={formData.address}  placeholder="address" required type="text" name="address" id="address" className="form-control" />
                    <label htmlFor="address">Address</label>
                </div>
                <div className="form-floating mb-3">
                    <input style={{ backgroundColor: "rgb(228, 230, 240)"}} onChange={handleFormChange} value={formData.phone_number}  placeholder="phone_number" required type="text" name="phone_number" id="phone_number" className="form-control" />
                    <label htmlFor="phone_number">Phone Number</label>
                </div>
                <button className="btn btn-secondary">Create</button>
                </form>
            </div>
            </div>
)};
export default NewCustomerForm;
