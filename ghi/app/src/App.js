import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import NewCustomerForm from './Sales/NewCustomerForm';
import ListCustomers from './Sales/ListCustomers';
import NewSalesPersonForm from './Sales/NewSalesPersonForm';
import ListSalesPeople from './Sales/ListSalesPeople';
import SalesPersonHistory from './Sales/SalesPersonHistory';
import SaleForm from './Sales/NewSale';
import ListSale from './Sales/ListSales';
import NewTechnicianForm from './Service/AddTechnician';
import TechnicianList from './Service/AllTechnicians';
import NewAppointmentForm from './Service/NewAppointment';
import AppointmentList from './Service/ListAppointments';
import ServiceHistory from './Service/ServiceHistory';
import NewManufacturerForm from './Inventory/NewManufacturer';
import ManufacturerList from './Inventory/ManufacturerList';
import NewAutomobileModelForm from './Inventory/CreateAutomobileModel';
import ModelList from './Inventory/VehicleModels';
import NewAutomobileForm from './Inventory/CreateAutomobile';
import AutomobileList from './Inventory/AutomobileList';
import "./index.css";
import Footer from "./Footer.js";

function App() {
    return (
        <BrowserRouter>
        <Nav />
            <div className="container">
                <Routes>
                    <Route path="/" element={<MainPage />} />
                    <Route path="/customers/new" element={<NewCustomerForm /> } />
                    <Route path="/salespeople/new" element={<NewSalesPersonForm /> } />
                    <Route path="/customers/list" element={<ListCustomers /> } />
                    <Route path="/salespeople/list" element={<ListSalesPeople /> } />
                    <Route path="/technicians/list" element={<TechnicianList /> } />
                    <Route path="/sales/new" element={<SaleForm /> } />
                    <Route path="/sales/list" element={<ListSale /> } />
                    <Route path="/salespeople/history" element={<SalesPersonHistory /> } />
                    <Route path="technicians/new" element={<NewTechnicianForm /> } />
                    <Route path="/appointments/new" element={<NewAppointmentForm /> } />
                    <Route path="/appointments/list" element={<AppointmentList /> } />
                    <Route path="/appointments/history" element={<ServiceHistory /> } />
                    <Route path="/manufacturers/list" element={<ManufacturerList /> } />
                    <Route path="/manufacturers/new" element={<NewManufacturerForm /> } />
                    <Route path="/models/list" element={<ModelList /> } />
                    <Route path="/models/new" element={<NewAutomobileModelForm /> } />
                    <Route path="/automobiles/list" element={<AutomobileList /> } />
                    <Route path="/automobiles/new" element={<NewAutomobileForm /> } />
                </Routes>
            </div>
        <Footer />
        </BrowserRouter>
    );
}

export default App;
