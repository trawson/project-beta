import React, { useState } from "react";


function NewTechnicianForm () {
    const[formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    });

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application.json'
            },
        };

        const response = await fetch (technicianUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                employee_id: '',
            });
        }
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <form onSubmit={handleSubmit} id="create-technician-gladys">
                    <h1 className="mb-3">
                        New Hire
                    </h1>
                    <div className="row">
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input style={{ backgroundColor: "rgb(228, 230, 240)"}} onChange={handleFormChange} value={formData.first_name} required placeholder="first_name" type="text" id="first_name" name="first_name" className="form-control"/>
                            <label htmlFor="name">First Name</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input style={{ backgroundColor: "rgb(228, 230, 240)"}} onChange={handleFormChange} value={formData.last_name} required placeholder="last_name" type="text" id="last_name" name="last_name" className="form-control"/>
                            <label htmlFor="style">Last Name</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input style={{ backgroundColor: "rgb(228, 230, 240)"}} onChange={handleFormChange} value={formData.employee_id} required placeholder="employee_id" type="number" id="employee_id" name="employee_id" className="form-control"/>
                            <label htmlFor="color">Employee ID</label>
                        </div>
                    </div>
                    </div>
                    <button className="btn btn-lg btn-secondary">Create!</button>
                </form>
            </div>
        </div>
    )
}

export default NewTechnicianForm;
